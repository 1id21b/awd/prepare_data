﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using awd_prepare_data.Workers;
using System.Collections.Generic;
using awd_prepare_data.Models;
using System;

namespace awd_prepare_data_Tests
{
    [TestClass]
    public class StatTests
    {
        [TestMethod]
        public void TestStat()
        {

            List<ReviewModel> reviews = new List<ReviewModel>();
            for (int i = 1; i < 11; i++)
            {
                reviews.Add(new ReviewModel { Id = i, Path = "does not matter", PreparedWordCount = i, UnpreparedWordCount = i + 2 });
            }

            //reviews.Add(new ReviewModel { Id = 1, Path = "does not matter", PreparedWordCount = 7, UnpreparedWordCount = 7 + 2 });
            //reviews.Add(new ReviewModel { Id = 2, Path = "does not matter", PreparedWordCount = 4, UnpreparedWordCount = 4 + 2 });
            //reviews.Add(new ReviewModel { Id = 3, Path = "does not matter", PreparedWordCount = -2, UnpreparedWordCount = -2 + 2 });

            StatisticWorker statisticWorker = new StatisticWorker(reviews);

            StatisticsModel statistics = statisticWorker.CalculateStatistics();

            int minPreEx = 1;
            int maxPreEx = 10;
            double avgPreEx = 5.5;
            double stdDevPreEx = 3.872983346207417;
            int gapPreEx = 5;
            int kw01Pre = 3;
            int kw09Pre = 10;
            double medianPre = 5.5;

            int minPreAcctual = statistics.PreMin;
            Assert.AreEqual(minPreEx, minPreAcctual, "Prawda");
            Assert.AreEqual(maxPreEx, statistics.PreMax);
            Assert.AreEqual(avgPreEx, statistics.PreAvg);
            Assert.AreEqual(stdDevPreEx, statistics.StdDeviationPre);
            Assert.AreEqual(gapPreEx, statistics.Gap);
            Assert.AreEqual(kw01Pre, statistics.Kwant01Pre);
            Assert.AreEqual(kw09Pre, statistics.Kwant09Pre);
            Assert.AreEqual(medianPre, statistics.PreMedian);
        }
        [TestMethod]
        public void Test3Numbers()
        {
            List<ReviewModel> reviews = new List<ReviewModel>();

            reviews.Add(new ReviewModel { Id = 1, Path = "does not matter", PreparedWordCount = 7, UnpreparedWordCount = 7 + 2 });
            reviews.Add(new ReviewModel { Id = 2, Path = "does not matter", PreparedWordCount = 4, UnpreparedWordCount = 4 + 2 });
            reviews.Add(new ReviewModel { Id = 3, Path = "does not matter", PreparedWordCount = -2, UnpreparedWordCount = -2 + 2 });

            StatisticWorker statisticWorker = new StatisticWorker(reviews);

            StatisticsModel statistics = statisticWorker.CalculateStatistics();

            int minPreEx = -2;
            int maxPreEx = 7;
            double avgPreEx = 3;
            double stdDevPreEx = Math.Sqrt(14);
            int gapPreEx = 3;
            int kw01Pre = -2;
            int kw09Pre = 7;
            double medianPre = 4;

            int minPreAcctual = statistics.PreMin;
            Assert.AreEqual(minPreEx, minPreAcctual, "Prawda");
            Assert.AreEqual(maxPreEx, statistics.PreMax);
            Assert.AreEqual(avgPreEx, statistics.PreAvg);
            Assert.AreEqual(stdDevPreEx, statistics.StdDeviationPre);
            Assert.AreEqual(gapPreEx, statistics.Gap);
            Assert.AreEqual(kw01Pre, statistics.Kwant01Pre);
            Assert.AreEqual(kw09Pre, statistics.Kwant09Pre);
            Assert.AreEqual(medianPre, statistics.PreMedian);
        }
    }
}
