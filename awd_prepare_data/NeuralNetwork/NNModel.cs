﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NumSharp;

namespace awd_prepare_data.NeuralNetwork
{
    class NNModel
    {

        public NDArray X;

        public NDArray y;

        public NNetwork nn;

        public bool notFirst = false;
        public List<Dictionary<int, int>> WordCountPairs;
        public List<double> Results;

        public NNModel()
        {
            X = np.array(new double[,] { { 0.0, 0.0, 1.0 },
                                         { 0.0, 1.0, 1.0 },
                                         { 1.0, 0.0, 1.0 },
                                         { 1.0, 1.0, 1.0 } });
            y = np.array(new double[,] { { 0d },
                                         { 1d },
                                         { 1d },
                                         { 0d } });
            Console.WriteLine(X.ToString());
            Console.WriteLine(y.ToString());
            nn = new NNetwork(X, y);
        }

        public NNModel(List<Dictionary<int, int>> wordCountPairs, Dictionary<string, int> WordCountPair, List<double> results)
        {
            //var tmp = WordCountPairs.OrderBy(w => w.Count).ToArray();

            WordCountPairs = wordCountPairs;
            Results = results;

            int size = WordCountPairs.Count - 1;
            size = size / 100;

            //TODO change matrix to contain all words and fill it in with data and check if it is crashing.
            NDArray matrix = np.array(new double[size, WordCountPair.Count]);
            NDArray vector = np.array(new double[size, 1]);


            //var tmpWCP = WordCountPair.ToArray();
            Random randy = new Random();
            int next = 0;
            for (int a = 0; a < size; a++)
            {
                next = randy.Next(WordCountPairs.Count - 1);
                vector[a, 0] = Results[next];

                var t = WordCountPairs[next];
                int j = 0;
                foreach (var tt in t)
                {
                    matrix[a, tt.Key] = tt.Value;
                    if (j != size)
                        j++;
                    else
                        break;
                }
                WordCountPairs.RemoveAt(next);
                Results.RemoveAt(next);
            }

            X = np.array(new double[,] { { 0.0, 0.0, 1.0, 1d, 0d }, { 0.0, 1.0, 1.0, 0d, 1d }, { 1.0, 0.0, 1.0, 1d, 1d }, { 1.0, 1.0, 1.0, 0d, 0d }, { 0.0, 1.0, 1.0, 0d, 0d }, { 1.0, 1.0, 0.0, 0d, 0d }, { 1.0, 1.0, 1.0, 1d, 1d } });
            y = np.array(new double[,] { { 0d }, { 1d }, { 1d }, { 0d }, { 0d }, { 1d }, { 1d } });

            Console.WriteLine(matrix.ToString());
            Console.WriteLine(vector.ToString());
            //Console.WriteLine(X.ToString());
            //Console.WriteLine(y.ToString());
            //if (!notFirst)
            //{
            nn = new NNetwork(matrix, vector);
            //    notFirst = true;
            //}
            //else
            //    nn = new NNetwork(matrix, vector, nn.weights1, nn.weights2);
        }

        public static NDArray sigmoid(NDArray x)
        {
            return 1.0 / (1 + np.exp(-x));
        }

        public static NDArray sigmoid_derivative(NDArray x)
        {
            return x * (1.0 - x);
        }

        public class NNetwork
        {
            public NDArray input;
            public NDArray weights1;
            public NDArray weights2;
            public NDArray y;
            private NDArray layer1;
            public NDArray output;

            public NNetwork(NDArray x, NDArray y)
            {
                this.input = x;
                this.weights1 = np.random.rand(this.input.shape[1], input.shape[0]);
                this.weights2 = np.random.rand(y.shape[0], 1);
                this.y = y;
                this.output = np.random.rand(this.y.shape);
            }

            public NNetwork(NDArray x, NDArray y, NDArray weights1, NDArray weights2)
            {
                this.input = x;
                this.weights1 = weights1;
                this.weights2 = weights2;
                this.y = y;
                this.output = np.random.rand(this.y.shape);
            }

            public virtual void feedforward()
            {
                this.layer1 = sigmoid(np.dot(this.input, this.weights1));
                this.output = sigmoid(np.dot(this.layer1, this.weights2));
            }

            public virtual void backprop()
            {
                // application of the chain rule to find derivative of the loss function with respect to weights2 and weights1
                var d_weights2 = np.dot(this.layer1.T, 2 * (this.y - this.output) * sigmoid_derivative(this.output));
                var d_weights1 = np.dot(this.input.T, np.dot(2 * (this.y - this.output) * sigmoid_derivative(this.output), this.weights2.T) * sigmoid_derivative(this.layer1));
                // update the weights with the derivative (slope) of the loss function
                this.weights1 += d_weights1;
                this.weights2 += d_weights2;
            }
        }

        public void trainTestModel()
        {

            for (int i = 0; i < 1000; i++)
            {
                nn.feedforward();
                nn.backprop();
            }
            Console.WriteLine("Have we learned anything? \n" + nn.output.ToString());
        }

        public void trainTestModel(List<Dictionary<int, int>> WordCountPairs, Dictionary<string, int> WordCountPair, List<double> results)
        {
            int size = 10;

            //TODO change matrix to contain all words and fill it in with data and check if it is crashing.
            NDArray matrix = np.array(new double[size, WordCountPair.Count]);
            NDArray vector = np.array(new double[size, 1]);


            //var tmpWCP = WordCountPair.ToArray();

            for (int a = 0; a < size; a++)
            {
                vector[a, 0] = results[a];
            }

            for (int i = 0; i < size; i++)
            {
                var t = WordCountPairs[i];
                foreach (var tt in t)
                {
                    matrix[i, tt.Key] = tt.Value;
                }
            }
            nn.input = matrix;
            nn.y = vector;

            for (int i = 0; i < 1000; i++)
            {
                nn.feedforward();
                nn.backprop();
            }
            Console.WriteLine("Have we learned anything? \n" + nn.output.ToString());
        }
    }
}
