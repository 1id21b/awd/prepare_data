﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace awd_prepare_data.NeuralNetwork
{
    class TrainModel : PrepareVector
    {

        private List<Dictionary<int, int>> Matrix;
        private List<double> ScoreVector;
        private NNModel m;
        public TrainModel(Dictionary<string, int> wCP, List<Dictionary<int, int>> _matrix, List<double> _scoreVector)
        {
            WordCountPair = wCP;
            Matrix = _matrix;
            ScoreVector = _scoreVector;
        }

        public TrainModel() { }

        public NNModel TrainNNModel()
        {

            for (int i = 0; i < Matrix.Count / 100; i++)
            {
                if (m == null)
                    m = new NNModel(Matrix, WordCountPair, ScoreVector);
                else
                    m = new NNModel(m.WordCountPairs, WordCountPair, m.Results);
                m.trainTestModel();
            }

            return m;

            //todo: try to learn in batches of 1 - 10 then go to the next with matrix of weigths from previious learning set;
        }
    }
}
