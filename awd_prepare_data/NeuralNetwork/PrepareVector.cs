﻿using Iveonik.Stemmers;
using LemmaSharp.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace awd_prepare_data.NeuralNetwork
{
    class PrepareVector
    {
        private int uniqueWordsCount = 0;
        private Dictionary<string, int> wordCountPair;
        private Dictionary<string, int> wordCountPairTest;

        public Dictionary<string, int> WordCountPair { get => wordCountPair; set => wordCountPair = value; }
        public Dictionary<string, int> WordCountPairTest { get => wordCountPairTest; set => wordCountPairTest = value; }
        public PrepareVector()
        {
            WordCountPair = new Dictionary<string, int>();
            WordCountPairTest = new Dictionary<string, int>();
            //WordCountPairs = new List<Dictionary<int, int>>();

        }

        public void createDictFromSingleComment(/*List<string> PreparedWords*/)
        {
            //Dictionary<string, int> WordCountPair = new Dictionary<string, int>();
            //foreach (var p in PreparedWords)
            //{
            //    if (!WordCountPair.Keys.Contains(p))
            //    {
            //        WordCountPair.Add(p, uniqueWordsCount++);
            //        //WordCountPairTest.Add(p, 1);
            //    }
            //    //else
            //    //{
            //    //    WordCountPairTest[p] = WordCountPairTest[p] + 1;
            //    //}
            //}
            //return WordCountPair;
            if (wordCountPair.Count == 0)
            {
                WordCountPair.Add("charact", 0);
                WordCountPair.Add("stori", 1);
                WordCountPair.Add("enjoy", 2);
                WordCountPair.Add("real", 3);
                WordCountPair.Add("love", 4);
            }


        }


        //TODO i have to go through comments one more time and get values for word occurances in each comment
        public List<Dictionary<int, int>> countWordsUsedinComment(List<string> PreparedWords)
        {
            List<Dictionary<int, int>> WordCountPairs = new List<Dictionary<int, int>>();
            Dictionary<int, int> tmp = new Dictionary<int, int>();
            //foreach (var p in PreparedWords)
            //{
            //    //var key = wordCountPair.Keys.Select((x, i) => new { Item = x, Index = i }).FirstOrDefault(k => k.Item == p);
            //    var key = WordCountPair.Where(k => k.Key == p).FirstOrDefault();
            //    //if (key.Key != null)
            //    //{
            //    if (!tmp.Keys.Contains(key.Value))
            //    {
            //        tmp.Add(key.Value, 1);
            //    }
            //    //}
            //}

            foreach (var w in WordCountPair)
            {
                if (PreparedWords.Contains(w.Key))
                {
                    tmp.Add(w.Value, 1);
                }
                else
                {
                    tmp.Add(w.Value, 0);
                }
            }

            WordCountPairs.Add(tmp);
            return WordCountPairs;
        }

        public List<string> MyStemmer(IStemmer stemmer, List<string> words)
        {
            List<string> stemmedWords = new List<string>();
            foreach (var word in words)
            {
                var stemmedWord = stemmer.Stem(word);
                if (words.Contains(stemmedWord))
                {
                    if (!stemmedWords.Contains(stemmedWord))
                        stemmedWords.Add(stemmedWord);
                }
                else
                {
                    if (!stemmedWords.Contains(stemmedWord))
                        stemmedWords.Add(stemmedWord);
                }
            }
            return stemmedWords;
        }
        public List<string> MyLemmer(Lemmatizer lemmer, List<string> words)
        {
            List<string> lemmedWords = new List<string>();
            foreach (var word in words)
            {
                var lemmedWord = lemmer.Lemmatize(word);
                if (words.Contains(lemmedWord))
                {
                    if (!lemmedWords.Contains(lemmedWord))
                        lemmedWords.Add(lemmedWord);
                }
                else
                {
                    if (!lemmedWords.Contains(lemmedWord))
                        lemmedWords.Add(lemmedWord);
                }
            }
            return lemmedWords;
        }


        public double CheckScore(double isGood)
        {
            if (isGood < 2.5)
            {
                return 0d;
            }
            else
            {
                return 1d;
            }
        }

    }
}
