﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace awd_prepare_data.Models
{
    class RegressionModel
    {
        //for y = ax + b
        private double a;
        private double b;

        public double A { get => a; set => a = value; }
        public double B { get => b; set => b = value; }
    }
}
