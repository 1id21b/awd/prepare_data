﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace awd_prepare_data.Models
{
    public class StatisticsModel
    {
        private int unMin;
        private int unMax;
        private int preMin;
        private int preMax;
        private double unAvg;
        private double preAvg;
        private double stdDeviation;
        private double stdDeviationPre;
        private double unMedian; //value int the middle of collection
        private double preMedian; //value int the middle of collection
        private int gap; //Value at 75% - Value at 25%
        private int gapPre; //Value at 75% - Value at 25%
        private double kwant01;
        private double kwant09;
        private double kwant01Pre;
        private double kwant09Pre;
        private double pearson;
        private double pearsonPre;

        public int UnMin { get => unMin; set => unMin = value; }
        public int PreMin { get => preMin; set => preMin = value; }
        public int UnMax { get => unMax; set => unMax = value; }
        public int PreMax { get => preMax; set => preMax = value; }
        public double UnAvg { get => unAvg; set => unAvg = value; }
        public double PreAvg { get => preAvg; set => preAvg = value; }
        public double StdDeviation { get => stdDeviation; set => stdDeviation = value; }
        public double StdDeviationPre { get => stdDeviationPre; set => stdDeviationPre = value; }
        public int Gap { get => gap; set => gap = value; }
        public int GapPre { get => gapPre; set => gapPre = value; }
        public double Kwant01 { get => kwant01; set => kwant01 = value; }
        public double Kwant09 { get => kwant09; set => kwant09 = value; }
        public double UnMedian { get => unMedian; set => unMedian = value; }
        public double PreMedian { get => preMedian; set => preMedian = value; }
        public double Kwant01Pre { get => kwant01Pre; set => kwant01Pre = value; }
        public double Kwant09Pre { get => kwant09Pre; set => kwant09Pre = value; }
        public double Pearson { get => pearson; set => pearson = value; }
        public double PearsonPre { get => pearsonPre; set => pearsonPre = value; }

        public override string ToString()
        {
            return $"Wartości dla Wszystkich słów:\n" +
                $"Min: {UnMin}\n" +
                $"Max: {UnMax}\n" +
                $"Średnia: {Math.Round(UnAvg, 2, MidpointRounding.AwayFromZero)}\n" +
                $"Odchylenie standardowe: {StdDeviation}\n" +
                $"Mediana: {UnMedian}\n" +
                $"Rozstęp międzykwartylowy: {Gap}\n" +
                $"Kwantyl rzędu 0.1: {Kwant01}\n" +
                $"Kwantyl rzędu 0.9: {Kwant09}\n" +
                $"Korelacja Pearsona: {Pearson}" +
                $"-----------------------------------------------------------------------------------------\n" +
                $"Wartości dla słów istotnych:\n" +
                $"Min: {PreMin}\n" +
                $"Max: {PreMax}\n" +
                $"Średnia: {Math.Round(PreAvg, 2, MidpointRounding.AwayFromZero)}\n" +
                $"Odchylenie standardowe: {StdDeviationPre}\n" +
                $"Mediana: {PreMedian}\n" +
                $"Rozstęp międzykwartylowy: {GapPre}\n" +
                $"Kwantyl rzędu 0.1: {Kwant01Pre}\n" +
                $"Kwantyl rzędu 0.9: {Kwant09Pre}\n" +
                $"Korelacja Pearsona: {PearsonPre}" +
                $"-----------------------------------------------------------------------------------------\n";
        }

    }
}
