﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace awd_prepare_data.Models
{
    public class ReviewModel
    {
        [Key]
        private int id;
        private string path;
        private int unpreparedWordCount;
        private int preparedWordCount;
        private double isGood;

        public int Id { get => id; set => id = value; }
        public string Path { get => path; set => path = value; }
        public int UnpreparedWordCount { get => unpreparedWordCount; set => unpreparedWordCount = value; }
        public int PreparedWordCount { get => preparedWordCount; set => preparedWordCount = value; }
        public double IsGood { get => isGood; set => isGood = value; } //values in range from 0 - 5
    }
}
