﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace awd_prepare_data.Models
{
    class JsonReviewModel
    {
        public string user { get; set; }
        public double stars { get; set; }
        public int time { get; set; }
        public string paid { get; set; }
        public string item { get; set; }
        public string review { get; set; }
    }
}
