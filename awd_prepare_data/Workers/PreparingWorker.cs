﻿using awd_prepare_data.Models;
using awd_prepare_data.NeuralNetwork;
using Iveonik.Stemmers;
using LemmaSharp.Classes;
using Newtonsoft.Json;
using StopWord;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace awd_prepare_data.Workers
{
    class PreparingWorker
    {
        private List<string> _textData;
        private double maxCount;
        private double current;
        private string _prepTextData;
        private List<ReviewModel> coments;
        private bool isRunning = false;
        public PreparingWorker(List<string> textData)
        {
            this.TextData = textData;
            Coments = new List<ReviewModel>();
            WordCountPairs = new List<Dictionary<int, int>>();
            Current = 0;
            MaxCount = 1000;
        }

        public List<string> TextData { get => _textData; set => _textData = value; }
        public string PrepTextData { get => _prepTextData; set => _prepTextData = value; }
        public bool IsRunning { get => isRunning; set => isRunning = value; }
        internal List<ReviewModel> Coments { get => coments; set => coments = value; }
        public double Current { get => current; set => current = value; }
        public double MaxCount { get => maxCount; set => maxCount = value; }
        public List<Dictionary<int, int>> WordCountPairs;

        public async void PrepareIMDbData()
        {
            int threads = Environment.ProcessorCount - 1;

            int PossibleJobs = 10;

            EnglishStemmer es = new EnglishStemmer();
            Lemmatizer lemmatizer = new Lemmatizer();

            ParallelOptions po = new ParallelOptions
            {
                MaxDegreeOfParallelism = threads
            };
            int concurrentThreads = 0;

            Random ran = new Random();

            isRunning = true;
            int numOfDataToTest = 1000;
            MaxCount = TextData.Count + numOfDataToTest;
            nGramWorker nGram = new nGramWorker();
            PrepareVector pVect = new PrepareVector();
            Console.WriteLine(Environment.ProcessorCount);
            var jobs = Enumerable.Range(0, PossibleJobs);


            List<double> scores = new List<double>();

            //for (int i = jobNr * (int)MaxCount / threads; i < jobNr * (int)MaxCount / threads + ((int)MaxCount / threads - 1); i++)

            int k = 0;
            foreach (var fileData in TextData)
            {
                Current++;
                var comment = new ReviewModel();
                comment.IsGood = 1;
                if (fileData.Contains("pos"))
                {
                    comment.IsGood = 5;
                }
                var data = File.ReadAllText(fileData);
                data = data.Replace("<br>", "").Replace("</br>", "").Replace("<br/>", "").Replace("<br />", "").Replace(",", "").Replace(".", "").Replace("!", "").Replace(":", "").Replace("?", "").Replace("'", "").Replace("\"", "").Replace("-", "").Replace("(", "").Replace(")", "").Replace("*", "").Replace("1", "").Replace("2", "").Replace("3", "").Replace("4", "").Replace("5", "").Replace("6", "").Replace("7", "").Replace("8", "").Replace("9", "").Replace("0", "").ToLower();
                PrepTextData = data.RemoveStopWords("en");
                nGram.analyseComment(data);
                var unpreparedWords = data.Split(' ').ToList();
                var preparedWords = PrepTextData.Split(' ').ToList();
                var stemmedWords = pVect.MyStemmer(es, preparedWords);
                var lemmedWords = pVect.MyLemmer(lemmatizer, stemmedWords);
                //if (Current <= numOfDataToTest && comment.IsGood == 1)
                //{
                //pVect.createDictFromSingleComment(lemmedWords);
                scores.Add(pVect.CheckScore(comment.IsGood));
                //scores.Add(ran.Next(0, 2));
                //}
                //if (k < numOfDataToTest && comment.IsGood == 5)
                //{
                //    k++;
                //    pVect.createDictFromSingleComment(lemmedWords);
                //    scores.Add(pVect.CheckScore(comment.IsGood));
                //    //scores.Add(ran.Next(0, 2));
                //}
                comment.Path = fileData;
                comment.UnpreparedWordCount = unpreparedWords.Count;
                comment.PreparedWordCount = preparedWords.Count;
                Coments.Add(comment);

            }
            pVect.createDictFromSingleComment();
            //scores = pVect.CheckScore();
            //var smth = pVect.WordCountPairTest.OrderByDescending(p => p.Value).ToList();

            //for (int j = 0; j < 100; j++)
            //{
            //    Console.WriteLine(smth.ElementAt(j));
            //}

            Parallel.ForEach(jobs, po, delegate (int jobNr)
            {
                EnglishStemmer esForParraller = new EnglishStemmer();
                Lemmatizer lemmatizerForParr = new Lemmatizer();

                int threadsRemaining = Interlocked.Increment(ref concurrentThreads);
                Console.WriteLine("[Job {0} is running. {1} threads running?", jobNr, threadsRemaining);

                //int jobNr = 0;
                int currentInThread = 0;

                List<Dictionary<int, int>> WordCountPairsTmp = new List<Dictionary<int, int>>();
                //foreach (var fileData in TextData)
                try
                {
                    int start = jobNr * ((numOfDataToTest) / PossibleJobs);
                    int end = ((jobNr * ((numOfDataToTest) / PossibleJobs)) + (numOfDataToTest / PossibleJobs));
                    Console.WriteLine("JobID: " + jobNr + " MaxCount " + 1000);
                    Console.WriteLine("Początkowe i; " + start);
                    Console.WriteLine("Końcowe i: " + end);
                    List<string> hundredComentsinwords = new List<string>();
                    for (int i = start; i < end; i++)
                    {
                        Current++;
                        var fileData = TextData[i];
                        var data = File.ReadAllText(fileData);
                        data = data.Replace("<br>", "").Replace("</br>", "").Replace("<br/>", "").Replace("<br />", "").Replace(",", "").Replace(".", "").Replace("!", "").Replace(":", "").Replace("?", "").Replace("'", "").Replace("\"", "").Replace("-", "").Replace("(", "").Replace(")", "").Replace("*", "").Replace("1", "").Replace("2", "").Replace("3", "").Replace("4", "").Replace("5", "").Replace("6", "").Replace("7", "").Replace("8", "").Replace("9", "").Replace("0", "").ToLower();
                        PrepTextData = data.RemoveStopWords("en");
                        var preparedWords = PrepTextData.Split(' ').ToList();
                        var stemmedWords = pVect.MyStemmer(esForParraller, preparedWords);
                        var lemmedWords = pVect.MyLemmer(lemmatizerForParr, stemmedWords);
                        var tmpList = pVect.countWordsUsedinComment(lemmedWords);
                        //if (fileData.Contains("pos"))
                        //{
                        //    scores.Add(1);
                        //}
                        //else
                        //    scores.Add(0);
                        //WordCountPairsTmp.AddRange(tmpList);
                        WordCountPairsTmp.AddRange(tmpList);
                        //if (i != 0 && i % 10 == 0)
                        //{
                        //    TrainModel model = new TrainModel(pVect.WordCountPair, WordCountPairs, scores);
                        //    model.TrainNNModel();
                        //    pVect.WordCountPair.Clear();
                        //    WordCountPairs.Clear();
                        //    scores.Clear();
                        //}
                        //if (current % 500 == 0)
                        //{
                        //    //TODO: Learning and reset memory on WordsUsed.
                        //    pVect.WordCountPairs.Clear();
                        //}
                    }

                    //for (int i = (TextData.Count / 2); i < (TextData.Count / 2) + (end - start); i++)
                    //{
                    //    Current++;
                    //    var fileData = TextData[i];
                    //    var data = File.ReadAllText(fileData);
                    //    data = data.Replace("<br>", "").Replace("</br>", "").Replace("<br/>", "").Replace("<br />", "").Replace(",", "").Replace(".", "").Replace("!", "").Replace(":", "").Replace("?", "").Replace("'", "").Replace("\"", "").Replace("-", "").Replace("(", "").Replace(")", "").Replace("*", "").Replace("1", "").Replace("2", "").Replace("3", "").Replace("4", "").Replace("5", "").Replace("6", "").Replace("7", "").Replace("8", "").Replace("9", "").Replace("0", "").ToLower();
                    //    PrepTextData = data.RemoveStopWords("en");
                    //    var preparedWords = PrepTextData.Split(' ').ToList();
                    //    var stemmedWords = pVect.MyStemmer(esForParraller, preparedWords);
                    //    var lemmedWords = pVect.MyLemmer(lemmatizerForParr, stemmedWords);
                    //    var tmpList = pVect.countWordsUsedinComment(lemmedWords);
                    //    if (fileData.Contains("pos"))
                    //    {
                    //        scores.Add(1);
                    //    }
                    //    else
                    //        scores.Add(0);
                    WordCountPairs.AddRange(WordCountPairsTmp);

                    //}
                    //TrainModel model = new TrainModel(pVect.WordCountPair, WordCountPairs, scores);
                    //model.TrainNNModel();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    Console.WriteLine("Job sie wyjebał: " + jobNr);
                }

                threadsRemaining = Interlocked.Decrement(ref concurrentThreads);
                Console.WriteLine("[Job {0} is done. {1} threads running", jobNr, threadsRemaining);
            });

            TrainModel model = new TrainModel(pVect.WordCountPair, WordCountPairs, scores);
            model.TrainNNModel();
            //var tmps = WordCountPairs.OrderBy(w => w.Count).ToArray();


            isRunning = false;
        }

        public async void PreparePiotrData()
        {
            isRunning = true;

            int threads = Environment.ProcessorCount - 1;

            int PossibleJobs = 25;

            ParallelOptions po = new ParallelOptions
            {
                MaxDegreeOfParallelism = threads
            };
            int concurrentThreads = 0;
            var jobs = Enumerable.Range(0, PossibleJobs);
            PrepareVector pVect = new PrepareVector();
            nGramWorker nGram = new nGramWorker();

            foreach (var fileData in TextData)
            {
                var data = File.ReadAllLines(fileData);
                //data = data.Replace("<br>", "").Replace("</br>", "").Replace("<br/>", "").Replace("<br />", "").ToLower();
                MaxCount = data.Count();
                //var eachComment = data.Split('\n');
                foreach (var com in data)
                {
                    Current++;
                    try
                    {
                        var currComment = JsonConvert.DeserializeObject<JsonReviewModel>(com);
                        var comment = new ReviewModel();
                        PrepTextData = currComment.review.RemoveStopWords("en");
                        nGram.analyseComment(currComment.review);
                        var unpreparedWords = currComment.review.Split(' ').ToList();
                        var preparedWords = PrepTextData.Split(' ').ToList();
                        //pVect.createDictFromSingleComment(preparedWords);
                        comment.Path = fileData;
                        comment.UnpreparedWordCount = unpreparedWords.Count;
                        comment.PreparedWordCount = preparedWords.Count;
                        comment.IsGood = currComment.stars;
                        Coments.Add(comment);
                    }
                    catch (Exception e)
                    {

                        continue;
                    }
                }
            }

            //Parallel.ForEach(jobs, po, delegate (int jobNr)
            //{
            //    int threadsRemaining = Interlocked.Increment(ref concurrentThreads);
            //    Console.WriteLine("[Job {0} is running. {1} threads running]", jobNr, threadsRemaining);

            //    //int jobNr = 0;
            //    int currentInThread = 0;

            //    List<Dictionary<int, int>> WordCountPairsTmp = new List<Dictionary<int, int>>();
            //    //foreach (var fileData in TextData)
            //    try
            //    {
            //        int start = jobNr * (TextData.Count / PossibleJobs);
            //        int end = ((jobNr * (TextData.Count / PossibleJobs)) + (TextData.Count / PossibleJobs));
            //        Console.WriteLine("JobID: " + jobNr + " MaxCount " + MaxCount);
            //        Console.WriteLine("Początkowe i; " + start);
            //        Console.WriteLine("Końcowe i: " + end);
            //        for (int i = start; i < end; i++)
            //        {
            //            Current++;
            //            var fileData = TextData[i];
            //            var data = File.ReadAllText(fileData);
            //            data = data.Replace("<br>", "").Replace("</br>", "").Replace("<br/>", "").Replace("<br />", "").Replace(",", "").Replace(".", "").Replace("!", "").Replace(":", "").Replace("?", "").Replace("'", "").Replace("\"", "").Replace("-", "").Replace("(", "").Replace(")", "").Replace("*", "").Replace("1", "").Replace("2", "").Replace("3", "").Replace("4", "").Replace("5", "").Replace("6", "").Replace("7", "").Replace("8", "").Replace("9", "").Replace("0", "").ToLower();
            //            PrepTextData = data.RemoveStopWords("en");
            //            var preparedWords = PrepTextData.Split(' ').ToList();
            //            var tmpList = pVect.countWordsUsedinComment(preparedWords);
            //            WordCountPairsTmp.AddRange(tmpList);
            //            //if (current % 500 == 0)
            //            //{
            //            //    //TODO: Learning and reset memory on WordsUsed.
            //            //    pVect.WordCountPairs.Clear();
            //            //}
            //        }
            //    }
            //    catch (Exception e)
            //    {
            //        //Console.WriteLine(e);
            //        Console.WriteLine("Job sie wyjebał: " + jobNr);
            //    }
            //    WordCountPairs.AddRange(WordCountPairsTmp);

            //    threadsRemaining = Interlocked.Decrement(ref concurrentThreads);
            //    Console.WriteLine("[Job {0} is done. {1} threads running", jobNr, threadsRemaining);
            //});

            isRunning = false;
        }

        public async Task<double> checkProgress(PreparingWorker worker)
        {
            Thread.Sleep(10);
            return Current / MaxCount * 100;
            //Sample_TxtBox.Text += "Wszystkie słowa: " + worker.UnWordCount + "\nWażne słowa: " + worker.PWordCount + "\n\n";
        }


    }
}
