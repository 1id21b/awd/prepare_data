﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace awd_prepare_data.Workers
{
    class nGramWorker
    {
        //private int[] dictIndexes;
        private Dictionary<(string, string), int> biGramDict;

        public Dictionary<(string, string), int> BiGramDict { get => biGramDict; set => biGramDict = value; }
        //public int[] DictIndexes { get => dictIndexes; set => dictIndexes = value; }

        public nGramWorker()
        {
            BiGramDict = new Dictionary<(string, string), int>();
        }

        public async void analyseComment(string text)
        {
            var words = text.Split(' ');
            int i = 0; 
            foreach (string word in words)
            {
                i++;
                if (i < words.Count())
                {
                    if (BiGramDict.ContainsKey((word, words[i])))
                    {
                        BiGramDict[(word, words[i])] = BiGramDict[(word, words[i])]++;
                    }
                    else
                    {
                        BiGramDict.Add((word, words[i]), 1);
                    }
                }
            }
        }
    }
}
