﻿using awd_prepare_data.Models;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace awd_prepare_data.Workers
{
    class HistogramWorker
    {

        public async Task<List<ColumnItem>> createHistogramFor(List<ReviewModel> reviews)
        {
            List<ColumnItem> barItems = new List<ColumnItem>();
            for (int i = 1; i <= 6; i++)
            {
                List<ReviewModel> currentReviews = reviews.Where(rev => rev.IsGood == i).OrderBy(rev => rev.PreparedWordCount).ToList();

                barItems.Add(new ColumnItem(currentReviews.Count,i-1));
            }

            return barItems;
        }

    }
}
