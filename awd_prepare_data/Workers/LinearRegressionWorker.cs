﻿using awd_prepare_data.Models;
using OxyPlot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace awd_prepare_data.Workers
{
    class LinearRegressionWorker
    {
        public async Task<RegressionModel> calculateRegression(List<ReviewModel> reviews)
        {
            //Our Y
            double sumY = reviews.Sum(r => r.PreparedWordCount);
            //Our X
            double sumX = reviews.Sum(r => r.UnpreparedWordCount);
            double sumSquaredY = reviews.Sum(r => r.PreparedWordCount * r.PreparedWordCount);
            double sumSquaredX = reviews.Sum(r => r.UnpreparedWordCount * r.UnpreparedWordCount);
            double ratioXandY = reviews.Sum(r => r.PreparedWordCount * r.UnpreparedWordCount);
            double count = reviews.Count;

            var licznikA = sumY * sumSquaredX - sumX * ratioXandY;
            var licznikB = count * ratioXandY - sumX * sumY;
            var mianownik = count * sumSquaredX - Math.Pow(sumX, 2);


            var a = licznikA / mianownik;
            var b = licznikB / mianownik;

            Console.WriteLine("A = " + a);
            Console.WriteLine("B = " + b);

            return new RegressionModel { A = a, B = b };

        }

        public async Task<List<DataPoint>> LineRegression(List<ReviewModel> rev)
        {
            List<DataPoint> dataPoints = new List<DataPoint>();
            rev = rev.OrderBy(r => r.UnpreparedWordCount).ToList();
            RegressionModel rm = await calculateRegression(rev);
            var yFirst = (rev.First().UnpreparedWordCount * rm.B) + rm.A;
            var yLAst = (rev.Last().UnpreparedWordCount * rm.B) + rm.A;
            var xFirst = rev.First().UnpreparedWordCount;
            var xLast = rev.Last().UnpreparedWordCount;
            dataPoints.Add(new DataPoint(xFirst, yFirst));
            dataPoints.Add(new DataPoint(xLast, yLAst));

            return dataPoints;
        }

    }
}
