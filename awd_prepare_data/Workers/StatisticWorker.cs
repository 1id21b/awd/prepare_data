﻿using awd_prepare_data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace awd_prepare_data.Workers
{
    public class StatisticWorker
    {
        private List<ReviewModel> coments;

        public StatisticWorker(List<ReviewModel> reviews)
        {
            Coments = reviews;
        }

        internal List<ReviewModel> Coments { get => coments; set => coments = value; }

        public StatisticsModel CalculateStatistics()
        {
            StatisticsModel statistics = new StatisticsModel();

            statistics.UnMin = Coments.Min(c => c.UnpreparedWordCount);
            statistics.UnMax = Coments.Max(c => c.UnpreparedWordCount);
            statistics.PreMax = Coments.Max(c => c.PreparedWordCount);
            statistics.PreMin = Coments.Min(c => c.PreparedWordCount);
            statistics.UnAvg = Coments.Average(c => c.UnpreparedWordCount);
            statistics.PreAvg = Coments.Average(c => c.PreparedWordCount);
            double licznik = 0;
            double licznikPre = 0;
            double licznikReview = 0;
            double reviewAvg = Coments.Average(c => c.IsGood);
            double licznikPearson = 0;
            double licznikPearsonPre = 0;
            int firstQuarter = (int)Math.Round((Coments.Count - 1) * 0.25, MidpointRounding.AwayFromZero);
            int thirdQuarter = (int)Math.Round((Coments.Count - 1) * 0.75, MidpointRounding.AwayFromZero);
            int kw01 = (int)Math.Round((Coments.Count - 1) * 0.1, MidpointRounding.AwayFromZero);
            int kw09 = (int)Math.Round((Coments.Count - 1) * 0.9, MidpointRounding.AwayFromZero);

            Coments = Coments.OrderBy(c => c.PreparedWordCount).ToList();
            foreach (var c in Coments)
            {
                licznik += Math.Pow((double)c.UnpreparedWordCount - statistics.UnAvg, 2);
                licznikPre += Math.Pow((double)c.PreparedWordCount - statistics.PreAvg, 2);
                licznikReview += Math.Pow(c.IsGood - reviewAvg, 2);

                licznikPearson += (c.UnpreparedWordCount - statistics.PreAvg) * (c.IsGood - reviewAvg);
                licznikPearsonPre += (c.PreparedWordCount - statistics.PreAvg) * (c.IsGood - reviewAvg);

            }
            statistics.StdDeviation = Math.Sqrt(licznik / statistics.UnAvg);
            statistics.StdDeviationPre = Math.Sqrt(licznikPre / statistics.PreAvg);

            statistics.Pearson = licznikPearson / ((Math.Sqrt(licznik)) * (Math.Sqrt(licznikReview)));
            statistics.PearsonPre = licznikPearsonPre / ((Math.Sqrt(licznikPre)) * (Math.Sqrt(licznikReview)));

            statistics.GapPre = Coments[thirdQuarter].PreparedWordCount - Coments[firstQuarter].PreparedWordCount;

            if (Coments.Count % 2 != 0)
            {
                statistics.PreMedian = Coments[(Coments.Count - 1) / 2].PreparedWordCount;
                statistics.Kwant01Pre = Coments[kw01].PreparedWordCount;
                statistics.Kwant09Pre = Coments[kw09].PreparedWordCount;
            }
            else
            {
                statistics.PreMedian = (Coments[(Coments.Count - 1) / 2].PreparedWordCount + Coments[(Coments.Count - 1) / 2 + 1].PreparedWordCount) / 2.0;
                statistics.Kwant01Pre = Coments[kw01 + 1].PreparedWordCount + Coments[kw01].PreparedWordCount / 2.0;
                statistics.Kwant09Pre = Coments[kw09 + 1].PreparedWordCount + Coments[kw01].PreparedWordCount / 2.0;
            }

            Coments = Coments.OrderBy(c => c.UnpreparedWordCount).ToList();
            statistics.Gap = Coments[thirdQuarter].UnpreparedWordCount - Coments[firstQuarter].UnpreparedWordCount;

            if (Coments.Count % 2 != 0)
            {
                statistics.UnMedian = Coments[(Coments.Count - 1) / 2].UnpreparedWordCount;
                statistics.Kwant01 = Coments[kw01].UnpreparedWordCount;
                statistics.Kwant09 = Coments[kw09].UnpreparedWordCount;

            }
            else
            {
                statistics.UnMedian = (Coments[(Coments.Count - 1) / 2].UnpreparedWordCount + Coments[(Coments.Count - 1) / 2 + 1].UnpreparedWordCount) / 2.0;
                statistics.Kwant01 = Coments[kw01 + 1].UnpreparedWordCount + Coments[kw01].UnpreparedWordCount / 2.0;
                statistics.Kwant09 = Coments[kw09 + 1].UnpreparedWordCount + Coments[kw01].UnpreparedWordCount / 2.0;

            }


            return statistics;
        }


    }
}
