﻿using awd_prepare_data.Models;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace awd_prepare_data.Workers
{
    class BoxPlotWorker
    {
        public async Task<List<BoxPlotItem>> calculateForBoxPlot(List<ReviewModel> reviews)
        {
            List<BoxPlotItem> plotItems = new List<BoxPlotItem>();
            List<ReviewModel> currentReviews;
            for (int i = 0; i < 7; i++)
            {
                currentReviews = reviews.Where(rev => rev.IsGood == i).OrderBy(rev => rev.PreparedWordCount).ToList();
                if (currentReviews.Any())
                {
                    int count = currentReviews.Count;
                    int r = count % 2;
                    double firstQuartil = GetMedian(currentReviews.Take((currentReviews.Count + r) / 2));
                    double thirdQuartil = GetMedian(currentReviews.Skip((currentReviews.Count - r) / 2));
                    double median = GetMedian(reviews);

                    double step = 0;
                    double upperWhisker = 0;
                    double lowerWhisker = 0;


                    var iqr = thirdQuartil - firstQuartil;
                    step = iqr * 1.5;
                    upperWhisker = thirdQuartil + step;
                    lowerWhisker = firstQuartil - step;

                    int uW = (int)upperWhisker;
                    int lW = (int)lowerWhisker;

                    int min = reviews.Last().PreparedWordCount;
                    int max = 0;
                    int j = 0;
                    foreach (var rev in reviews)
                    {
                        if (rev.PreparedWordCount <= upperWhisker)
                        {
                            if (max < rev.PreparedWordCount)
                                max = rev.PreparedWordCount;
                        }
                        if (rev.PreparedWordCount >= lowerWhisker)
                        {
                            if (min > rev.PreparedWordCount)
                                min = rev.PreparedWordCount;
                        }
                    }

                    upperWhisker = max;
                    lowerWhisker = min;

                    plotItems.Add(new BoxPlotItem(i, lowerWhisker, firstQuartil, median, thirdQuartil, upperWhisker));
                    currentReviews.Clear();
                }
                else
                    plotItems.Add(new BoxPlotItem(i, 0, 0, 0, 0, 0));
            }

            return plotItems;
        }

        private static double GetMedian(IEnumerable<ReviewModel> values)
        {
            var sortedInterval = new List<int>(values.Select(r =>r.PreparedWordCount));
            sortedInterval.Sort();
            var count = sortedInterval.Count;
            if (count % 2 == 1)
            {
                return sortedInterval[(count - 1) / 2];
            }

            return 0.5 * sortedInterval[count / 2] + 0.5 * sortedInterval[(count / 2) - 1];
        }
    }
}
