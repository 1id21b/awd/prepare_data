﻿using awd_prepare_data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace awd_prepare_data.Workers
{
    public class OutliersWorker
    {

        private List<ReviewModel> _reviews;

        public OutliersWorker(List<ReviewModel> reviews)
        {
            _reviews = reviews;
        }

        public List<ReviewModel> FindOutliers(StatisticsModel statistics)
        {
            List<ReviewModel> outliers = new List<ReviewModel>();

            foreach (var review in _reviews)
            {
                if (review.UnpreparedWordCount < statistics.Kwant01 || review.UnpreparedWordCount > statistics.Kwant09 || review.PreparedWordCount < statistics.Kwant01Pre || review.PreparedWordCount > statistics.Kwant09Pre)
                {
                    outliers.Add(review);
                }
            }
            outliers.ForEach(o => _reviews.Remove(o));

            return outliers;
        }

    }
}