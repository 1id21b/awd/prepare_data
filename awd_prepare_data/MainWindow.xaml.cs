﻿using awd_prepare_data.Models;
using awd_prepare_data.Workers;
using Microsoft.Win32;
using OxyPlot;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace awd_prepare_data
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private List<string> _data;
        private List<ReviewModel> _reviews;
        private StatisticsModel statistics;

        public List<DataPoint> Points { get; private set; }
        public List<BoxPlotItem> boxItems { get; private set; }
        public List<ColumnItem> barItems { get; private set; }
        public List<DataPoint> Line { get; private set; }

        public MainWindow()
        {
            InitializeComponent();
            _reviews = new List<ReviewModel>();
        }

        private void btnOpenFile_Click(object sender, RoutedEventArgs e)
        {

            _data = new List<string>();
            if (string.IsNullOrEmpty(FilePath_TxtBox.Text))
            {
                FolderBrowserDialog openFileDialog = new FolderBrowserDialog();
                if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    var dir = Directory.GetDirectories(openFileDialog.SelectedPath).ToList();
                    foreach (var d in dir)
                    {
                        _data.AddRange(Directory.GetFiles(d, "*.txt").ToList());
                    }
                    if (_data.Count == 0)
                    {
                        foreach (var directory in dir)
                        {
                            _data.AddRange(Directory.GetFiles(directory, "*.json").ToList());
                        }
                    }

                    FilePath_TxtBox.Text = openFileDialog.SelectedPath;
                    //foreach (var fileName in _data)
                    //    Sample_TxtBox.Text += fileName + "\n";
                }
            }
            else
            {
                var dir = Directory.GetDirectories(FilePath_TxtBox.Text).ToList();
                foreach (var d in dir)
                {
                    _data.AddRange(Directory.GetFiles(d, "*.txt").ToList());
                }
                if (_data.Count == 0)
                {
                    foreach (var directory in dir)
                    {
                        _data.AddRange(Directory.GetFiles(directory, "*.json").ToList());
                    }
                }
                //foreach (var fileName in _data)
                //    Sample_TxtBox.Text += fileName + "\n";
            }
            //Sample_TxtBox.Text = _data;
        }

        private async void btnPrepareData_Click(object sender, RoutedEventArgs e)
        {
            Sample_TxtBox.Text = "";

            if (_data != null)
            {
                PreparingWorker worker = new PreparingWorker(_data);

                if (_data.FirstOrDefault().Contains(".txt"))
                    Task.Run(() => worker.PrepareIMDbData());
                else
                    Task.Run(() => worker.PreparePiotrData());

                bool t = true;
                Thread.Sleep(500);
                while (t)
                {
                    pBar.Value = await Task.Run(() => worker.checkProgress(worker));
                    t = worker.IsRunning;
                }
                _reviews.AddRange(worker.Coments);

            }
            else
            {
                var result = System.Windows.Forms.MessageBox.Show("Najpierw wczytaj pliki", "Informacja", MessageBoxButtons.OK);
                //if(result)
            }


            //Thread.Sleep(500);
            //Sample_TxtBox.Text += "I'm done!";
            //Sample_TxtBox.Text += worker.TextData + "\n\n";
            //Sample_TxtBox.Text += worker.PrepTextData;
        }

        private async void btnCalculateStats_Click(object sender, RoutedEventArgs e)
        {
            if (_reviews != null && _reviews.Count > 0)
            {
                StatisticWorker statisticWorker = new StatisticWorker(_reviews);
                statistics = statisticWorker.CalculateStatistics();
                Sample_TxtBox.Text = statistics.ToString();

                Sample_TxtBox.Visibility = Visibility.Visible;
                RegressionPlot.Visibility = Visibility.Hidden;
                BoxPlot.Visibility = Visibility.Hidden;
                Histogram.Visibility = Visibility.Hidden;
            }
            else
            {
                var result = System.Windows.Forms.MessageBox.Show("Najpierw przygotuj dane", "Informacja", MessageBoxButtons.OK);
                //if(result)
            }

        }

        private async void btnFindOutliers_Click(object sender, RoutedEventArgs e)
        {
            OutliersWorker outliersWorker = new OutliersWorker(_reviews);
            if (statistics != null)
            {
                var outliers = outliersWorker.FindOutliers(statistics);
                Sample_TxtBox.Text = outliers.Count.ToString();
            }
            else
            {
                var result = System.Windows.Forms.MessageBox.Show("Najpierw wygeneruj statystykę", "Informacja", MessageBoxButtons.OK);
                //if(result)
            }
        }

        private async void btnShowGraph_Click(object sender, RoutedEventArgs e)
        {
            if (_reviews != null && _reviews.Count > 0)
            {
                LinearRegressionWorker lrw = new LinearRegressionWorker();

                Points = new List<DataPoint>();

                foreach (var r in _reviews)
                {
                    Points.Add(new DataPoint(r.UnpreparedWordCount, r.PreparedWordCount));
                }

                RegressPoints.ItemsSource = Points;
                Line = await lrw.LineRegression(_reviews);

                RegressLine.ItemsSource = Line;

                RegressionPlot.Visibility = Visibility.Visible;
                Sample_TxtBox.Visibility = Visibility.Hidden;
                BoxPlot.Visibility = Visibility.Hidden;
                Histogram.Visibility = Visibility.Hidden;
            }
            else
            {
                var result = System.Windows.Forms.MessageBox.Show("Najpierw przygotuj dane", "Informacja", MessageBoxButtons.OK);
            }
        }

        private async void btnShowBoxGraph_Click(object sender, RoutedEventArgs e)
        {
            if (_reviews != null && _reviews.Count > 0)
            {
                BoxPlotWorker lrw = new BoxPlotWorker();

                boxItems = new List<BoxPlotItem>();

                boxItems = await lrw.calculateForBoxPlot(_reviews);

                BoxPlotGraph.ItemsSource = boxItems;

                BoxPlot.Visibility = Visibility.Visible;
                RegressionPlot.Visibility = Visibility.Hidden;
                Sample_TxtBox.Visibility = Visibility.Hidden;
                Histogram.Visibility = Visibility.Hidden;
            }
            else
            {
                var result = System.Windows.Forms.MessageBox.Show("Najpierw przygotuj dane", "Informacja", MessageBoxButtons.OK);
            }
        }

        private async void btnShowHistGraph_Click(object sender, RoutedEventArgs e)
        {
            if (_reviews != null && _reviews.Count > 0)
            {
                HistogramWorker hW = new HistogramWorker();

                barItems = new List<ColumnItem>();

                barItems = await hW.createHistogramFor(_reviews);

                HistSeries.ItemsSource = barItems;

                Histogram.Visibility = Visibility.Visible;
                BoxPlot.Visibility = Visibility.Hidden;
                RegressionPlot.Visibility = Visibility.Hidden;
                Sample_TxtBox.Visibility = Visibility.Hidden;
            }
            else
            {
                var result = System.Windows.Forms.MessageBox.Show("Najpierw przygotuj dane", "Informacja", MessageBoxButtons.OK);
            }
        }

    }
}
